import customtkinter
import customtkinter as ctk
from PIL import Image
import os



customtkinter.set_appearance_mode("dark") #MODO DE LA VENTANA (OBSCURA)


class App(customtkinter.CTk):#DIMENSIONES DE PANTALLA
    width = 700
    height = 600

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        
        #PROPIEDADES DE LA PANTALLA
        self.title("LOGIN")
        self.geometry(f"{self.width}x{self.height}")
        self.resizable(False, False)#NO SE VA REDIMENSIONAR, TENDRA UNA MEDIDA AJUSTADA

        # lMAGEN DE FONDO
        current_path = os.path.dirname(os.path.realpath(__file__))
        self.bg_image = customtkinter.CTkImage(Image.open(current_path + "./imagenes/InicioSesion.png"),#IMPORTAR LA IMAGEN
                                               size=(self.width, self.height))#HEREDAR MEDIDAS DE LA VENTANA

       #TEXTO
        self.bg_image_label=customtkinter.CTkLabel(self,image=self.bg_image, text="INICIO DE SESION",
                                                    font=customtkinter.CTkFont(size=15, weight="bold"))
        self.bg_image_label.grid(row=0, column=0, pady=(0,350))
        
        #USUARIO
        self.username_entry = customtkinter.CTkEntry(self, width=200, placeholder_text="USUARIO",
                                                     font=customtkinter.CTkFont(size=10, weight="bold"))
        self.username_entry.grid(row=0, column=0, pady=(0, 250))
        
        #CONTRASEÑA
        self.password_entry = customtkinter.CTkEntry(self,width=200, show="*",
                                                    placeholder_text="CONTRASEÑA",
                                                    fg_color="transparent",
                                                    font=customtkinter.CTkFont(size=10, weight="bold"))
        self.password_entry.grid(row=0, column=0, pady=(0, 180))
        
        #BOTON (INGRESAR)
        self.login_button = customtkinter.CTkButton(self, text="INGRESAR", command=self.login_event, width=200)
        self.login_button.grid(row=0, column=0, pady=(0, 100))
        
        #TEXTO
        self.bg_image_label2=customtkinter.CTkLabel(self, text="",
                                                    font=customtkinter.CTkFont(size=30, weight="bold"))
        self.bg_image_label2.grid(row=0, column=0, pady=(0,0))
        
        # create main frame
        self.main_frame = customtkinter.CTkFrame(self, corner_radius=0)
        self.main_frame.grid_columnconfigure(0, weight=1)
        self.main_label = customtkinter.CTkLabel(self.main_frame, text="NUEVA VENTANA",
                                                 font=customtkinter.CTkFont(size=20, weight="bold"))
        self.main_label.grid(row=0, column=0, padx=30, pady=(30, 15))

#ACCION DEL BOTON
    def login_event(self):
        print("Login pressed - username:", self.username_entry.get(), "password:", self.password_entry.get())

        self.main_frame.grid_forget()  # MOVERSE AL FRAME
        self.main_frame.grid(row=0, column=0, sticky="nsew", padx=100)  # POSICINAR EN EL FRAME

if __name__ == "__main__":
    app = App()
    app.mainloop()
